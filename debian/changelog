ruby-tokyocabinet (1.31-7) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

  [ Jérôme Charaoui ]
  * d/control: drop obsolete X?-Ruby-Versions
  * d/control: use ${ruby:Depends} substvar
  * d/patches: add patch to fix ftbfs with gcc-14 (Closes: #1075481)

 -- Jérôme Charaoui <jerome@riseup.net>  Fri, 26 Jul 2024 21:11:06 -0400

ruby-tokyocabinet (1.31-6) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Remove patches missing from debian/patches/series.
  * Change priority extra to priority optional.

  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files out of the source package

  [ Debian Janitor ]
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Bump debhelper from old 12 to 13.

  [ Lucas Nussbaum ]
  * Refresh packaging using dh-make-ruby -w

 -- Lucas Nussbaum <lucas@debian.org>  Thu, 14 Apr 2022 20:54:42 +0200

ruby-tokyocabinet (1.31-5) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Christian Hofstaedtler ]
  * Patch gemspec to make it loadable by gem2deb, fixes FTBFS.

 -- Christian Hofstaedtler <zeha@debian.org>  Tue, 22 Mar 2016 12:17:24 +0000

ruby-tokyocabinet (1.31-4) unstable; urgency=medium

  * Team upload

  [ Christian Hofstaedtler ]
  * Fix uninitialized constant `Config` (Closes: #791779)

  [ Cédric Boutillier ]
  * Refresh packaging with 'dh-make-ruby -w'

 -- Christian Hofstaedtler <zeha@debian.org>  Fri, 14 Aug 2015 14:32:11 +0200

ruby-tokyocabinet (1.31-3) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0 official URL for Format field

  [ Laurent Arnoud ]
  * d/control: bumped up standards version to 3.9.5

  [ Christian Hofstaedtler ]
  * Remove transitional packages
  * Rebuild for currently supported Ruby versions (Closes: #735736)

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 06 Feb 2014 21:15:48 +0100

ruby-tokyocabinet (1.31-2) unstable; urgency=low

  * debian/ruby-tests.rb: use includes provided by test runner
  * Add Lucas to Uploaders:
  * Update Standards-Version and dependency on ruby
  * Add Replaces, Breaks, Provides
  * Remove ./tokyocabinet.so before running tests
  * Drop change_require_statements_using_relative_paths.patch (Closes: #634439)

 -- Lucas Nussbaum <lucas@debian.org>  Sat, 30 Jul 2011 08:59:03 +0200

ruby-tokyocabinet (1.31-1) unstable; urgency=low

  * New upstream release (copyright change).
  * Switch to gem2deb-based packaging. Source and binary package renamed to
    ruby-tokyocabinet. Transitional packages added.

 -- Laurent Arnoud <laurent@spkdev.net>  Sun, 17 Apr 2011 22:24:33 +0200

tokyocabinet-ruby (1.30-2) unstable; urgency=low

  [ Laurent Arnoud ]
  * Added support for Ruby 1.9.2 (Closes: #593024)
  * Bump Standards-Version to 3.9.1 (no changes).
  * Update Vcs-* Fields.
  * Update upstream website, copyright and upstream author

  [ Paul van Tilburg ]
  * Added the Debian/Ruby Extras team to the Uploaders field; the package
    is now team mainted.

 -- Laurent Arnoud <laurent@spkdev.net>  Thu, 16 Sep 2010 21:55:04 +0200

tokyocabinet-ruby (1.30-1) unstable; urgency=low

  * New upstream release
  * Generate rdoc for libtokyocabinet-ruby-doc.
  * Bump Standards-Version to 3.8.4 (no changes).
  * Switched to source format 3.0 (quilt)
  * debian/control: Change my email address.
  * Add Vcs fields.

 -- Laurent Arnoud <laurent@spkdev.net>  Wed, 24 Mar 2010 20:55:30 +0100

tokyocabinet-ruby (1.29-2) unstable; urgency=low

  * Drop 1.9 package (Closes: #565838)
  * Enable the test suite

 -- Laurent Arnoud <laurent.arnoud@gmail.com>  Tue, 19 Jan 2010 22:13:40 +0100

tokyocabinet-ruby (1.29-1) unstable; urgency=low

  * New upstream release
  * Add ruby 1.9.1 version

 -- Laurent Arnoud <laurent.arnoud@gmail.com>  Mon, 09 Nov 2009 21:40:34 +0100

tokyocabinet-ruby (1.21-1) unstable; urgency=low

  * Initial release (Closes: #551930)

 -- Laurent Arnoud <laurent.arnoud@gmail.com>  Tue, 20 Oct 2009 23:55:13 +0200
